#include "header.h"

using namespace std;

//prvi

void unos_niza(int* niz, int n) 
{
	cout << "Unesite clanove niza:";
	for (int i = 0; i < n; i++)
		cin >> niz[i];
}

void min_max(int *niz, int& min, int& max, int n)
{
	for (int i = 0; i < n; i++)
	{
		if (niz[i] < min)
			min = niz[i];
		if (niz[i] > max)
			max = niz[i];
	}
}

//drugi

int& vraca_ref (int* niz, int element)
{
	return niz[element];
}


//treci

struct pravokutnik {
	int x1, y1, x2, y2;
};

struct kruznica {
	int polumjer, x, y;
};

int presjecanje(pravokutnik p, kruznica k)
{
	int v, s, ux, uy, brojac=0;
	v=abs(p.x2-p.x1)/2;
	s=abs(p.y1-p.y2)/2;
	ux=abs(k.x-p.x1);
	uy=abs(k.y-p.y1);
	if (ux<=v || uy<=s)
		brojac++;
	return brojac;
}


//cetvrti

struct Vektor{
        int *niz;
        size_t log_vel, fiz_vel;
        
        void vector_new (int init);
        void vector_del (Vektor &novi);
        void push_back (Vektor novi, double &broj, int init);
        void pop_back (Vektor &novi);
        int vector_front (Vektor novi); 
        int vector_back (Vektor novi);
        size_t vector_size (Vektor novi);

		void print_vector();
};

void Vektor::vector_new (int init)
{
    Vektor nova;
    nova.niz=new int[init];
    nova.fiz_vel=init;
    nova.log_vel=0;
}

void Vektor::vector_del (Vektor &novi)
{
    delete [] novi.niz;
    novi.fiz_vel=0;
    novi.log_vel=0;
    
}

//provjeriti je li niz ispunjen i uve�at za duplo ako je, svaki put 

void Vektor::push_back (Vektor novi, int &broj, int init)
{
    int i;
	if (novi.log_vel==novi.fiz_vel){
		int *temp=novi.niz;
		novi.niz=new int[2*novi.fiz_vel];
		for (i=0; i<novi.fiz_vel; i++)
        	novi.niz[i]=temp[i];
    }
    delete [] temp;
    novi.fiz_vel++;
}

void Vektor::pop_back (Vektor &novi)
{
    novi.fiz_vel=novi.fiz_vel-1;
}

int Vektor::vector_front (Vektor novi)
{
    return novi.niz[0];
}

int Vektor::vector_back (Vektor novi)
{
    return novi.niz[novi.fiz_vel-1];
}

int Vektor::vector_size (Vektor novi)
{
    if (novi.fiz_vel==0)
        return -1;
    return novi.fiz_vel;
}




